import React from 'react';
import { Typography, makeStyles, List, ListItem } from '@material-ui/core';
import { useTableOfContentsContext } from './context';

const useStyles = makeStyles((theme) => ({
  TableOfContents: {
    top: '70px',
    width: '175px',
    height: 'calc(100vh - 70px)',
    display: 'none',
    padding: '16px 16px 16px 0px',
    position: 'sticky',
    marginTop: '86px',
    overflowY: 'auto',
    flexShrink: 0,
    [theme.breakpoints.up('md')]: {
      display: 'block',
    },
  },
  listItem: {
    padding: 0,
  },
  listName: {
    color: theme.palette.text.secondary,
  },
}));

export default function TableOfContents() {
  const classes = useStyles();
  const tableOfContentsContext = useTableOfContentsContext();

  const items = tableOfContentsContext.content.map((item) => (
    <ListItem key={item.name} className={classes.listItem}>
      <a href={item.link}>
        <span className={classes.listName}>{item.name}</span>
      </a>
    </ListItem>
  ));

  if (tableOfContentsContext.content.length <= 0) {
    return <></>;
  }

  return (
    <nav className={classes.TableOfContents}>
      <Typography variant="body1">Inhoud</Typography>
      <List>{items}</List>
    </nav>
  );
}
