interface Route {
  name: string;
  children?: Route[];
}

const routes: Route[] = [{ name: 'Gidslijn' }, { name: 'Liften' }, { name: 'Rotonde' }, { name: 'Begeleiden' }];

export default routes;
