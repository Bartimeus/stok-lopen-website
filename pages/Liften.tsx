import React, { useEffect } from 'react';
import { List, ListItem, ListItemText } from '@material-ui/core';
import Description from '../src/components/typography/Description';
import Image from '../src/components/typography/image';
import H1 from '../src/components/typography/H1';
import H2 from '../src/components/typography/H2';
import { useTableOfContentsContext } from '../src/components/TableOfContents/context';

import Afbeelding1 from './afbeeldingen/liften/Afbeelding1.png?webp';
import Afbeelding2 from './afbeeldingen/liften/Afbeelding2.png?webp';
import Afbeelding3 from './afbeeldingen/liften/Afbeelding3.png?webp';
import Afbeelding4 from './afbeeldingen/liften/Afbeelding4.png?webp';
import Afbeelding5 from './afbeeldingen/liften/Afbeelding5.png?webp';
import Afbeelding6 from './afbeeldingen/liften/Afbeelding6.png?webp';

export default function Liften() {
  const tableOfContentsContext = useTableOfContentsContext();

  useEffect(() => {
    tableOfContentsContext.newContent([
      { name: 'Liften', link: '/Liften#liften' },
      { name: 'Gebruik liften', link: '/Liften#gebruik-liften' },
    ]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <H1>Liften</H1>
      <Description>
        Om een lift te kunnen gebruiken is het een voorwaarde dat je de lift kent; plek van het bedieningspaneel, knoppen enz Daarom is het
        noodzakelijk om samen met de O&M instructeur de omstandigheden in en rond de lift te onderzoeken. Het is noodzakelijk om te weten
        waar het bedieningspaneel zich bevindt, waar de stopknop en noodknop zijn, zijn er braille tekens op de knoppen of is er spraak
        ondersteuning. Hoeveel etages heeft het gebouw. Heeft de deur sensoren zodat ze waar te nemen of er zich nog iets tussen de deur
        bevindt?.
      </Description>
      <Image src={Afbeelding1} alt="Hand die op begane grond knop drukt in lift" />
      <H2>Gebruik liften</H2>
      <List component="ol" type="1">
        <ListItem>
          <ListItemText>
            De cliënt kan de lift lokaliseren door te luisteren naar de geluiden die bij een lift horen . Het kan een bel zijn, een deur die
            opengaat, het geluid van het lopen van andere voetgangers, of een geleidelijn richting de ingang van de lift. Het kan soms nodig
            zijn om anderen om hulp te vragen.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            De knoppen bevinden zich meestal op de muurlinks of rechts van de liftdeur.
            <Image src={Afbeelding2} alt="Vrouw klikt op knop om lift te laten komen" />
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Als de lift aankomt, maakt De cliënt ruimte om mensen uit de lift te laten stappen.
            <Image src={Afbeelding3} alt="Vrouw gaat aan de kant voor een oudere mevrouw" />
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Bij het instappen van de lift is het een goed idee om de ‘korte stok’ te gebruiken. De cliënt onderzoekt de vloer van de lift
            met een pendel beweging om zeker te weten dat dit de lift er is. Je kunt eventueel de beschermende techniek (hoge arm) gebruiken
            om jezelf te beveiligen tegen deuren die sluiten.
            <Image src={Afbeelding4} alt="Vrouw stapt lift in met stok naar voren" />
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            In de lift gaat De cliënt met gezicht richting de deur staan, ze lokaliseert het bedieningspaneel en activeert de juiste knop.
            Het bedieningspaneel zit meestal aan de rechterkant bij binnenkomst maar kan ook links zitten!
            <Image src={Afbeelding5} alt="Vrouw staat in lift en drukt op knop voor begane grond" />
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Als er geen voelbare markering op het bedieningspaneel zit, kun je bijvoorbeeld de knoppen tellen. Soms is het mogelijk om
            toestemming te krijgen van de beheerder van de lift/het gebouw om een vorm van markering op het bedieningspaneel zelf aan te
            brengen.(als je deze lift vaak gebruikt)
            <Image src={Afbeelding1} alt="Hand die op begane grond knop drukt in lift" />
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Als de verdiepingen niet wordten uitgesproken moet de cliënt op een andere manier controleren of ze op de juiste verdieping is
            gekomen. Soms moet ze de lift hiervoor verlaten om naar een oriëntatiepunt te zoeken. Als er auditieve indicatoren zijn kan ze
            de etages tellen, of misschien kan ze de tijd op meten. Indien technisch mogelijk, kan ze alle knoppen voor de tussenliggende
            etages indrukken om zo te kunnen tellen.
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Bij het verlaten van de lift is het belangrijk dat de stok contact heeft met de vloer buiten om te controleren dat de vloer van
            de lift op gelijke hoogte is met de verdieping. Het is ook hier verstandig om een korte stok te gebruiken bij het pendelen om te
            voorkomen dat de mensen die staan te wachten op de lift geraakt worden door de stok.
            <Image src={Afbeelding6} alt="Vrouw verlaat de lift met stok naar voren" />
          </ListItemText>
        </ListItem>
      </List>
    </>
  );
}
