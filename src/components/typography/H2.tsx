import React from 'react';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  H2: {
    margin: '40px 0 16px',
    fontSize: '30px',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    fontWeight: 400,
    lineHeight: 1.235,
    letterSpacing: '0.00735em',
  },
  anchorLink: {
    marginTop: '-96px',
    position: 'absolute',
  },
});

interface Props {
  children: string | undefined;
}

export default function H2(props: Props) {
  const classes = useStyles();
  const anchorLinkText = props.children.toLowerCase().replace(/ /g, '-');

  return (
    <h2 className={classes.H2}>
      {/* eslint-disable-next-line jsx-a11y/anchor-is-valid, jsx-a11y/anchor-has-content */}
      <a className={classes.anchorLink} id={anchorLinkText} aria-hidden="true" />
      {props.children}
    </h2>
  );
}
