import React, { ReactNode } from 'react';
import { makeStyles } from '@material-ui/core';
import theme from '../../theme';

const useStyles = makeStyles({
  description: {
    fontSize: '1.2rem',
    marginBottom: theme.spacing(2),
    fontWeight: 400,
    lineHeight: 1.334,
    letterSpacing: '0em',
  },
});

interface Props {
  children: ReactNode | ReactNode[] | string | undefined;
}

export default function Description(props: Props) {
  const classes = useStyles();

  return <p className={classes.description}>{props.children}</p>;
}
