module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: ['./tsconfig.json'],
  },
  plugins: ['@typescript-eslint', 'jest'],
  extends: [
    'airbnb-typescript',
    'airbnb/hooks',
    'prettier/@typescript-eslint',
    'prettier/react',
    'plugin:jest/recommended',
    'plugin:prettier/recommended',
  ],
  rules: {
    'react/destructuring-assignment': [2, 'never'],
    'import/no-unused-modules': 0,
    'eslintglobal-require': 0,
  },
  env: {
    browser: true,
    node: true,
    jest: true,
  },
};
