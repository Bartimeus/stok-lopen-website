import React from 'react';
import Head from 'next/head';
import { ThemeProvider, makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { AppProps } from 'next/app';
import theme from '../src/theme';
import Drawer from '../src/components/Drawer/Component';
import TableOfContents from '../src/components/TableOfContents/Component';
import { TableOfContentsProvider } from '../src/components/TableOfContents/context';
import BottomNavigation from '../src/components/BottomNavigation/Component';

const useStyles = makeStyles((styleTheme) => ({
  root: {
    display: 'flex',
  },
  contentWrapper: {
    display: 'flex',
    margin: '0 auto',
  },
  content: {
    position: 'relative',
    display: 'block',
    width: 'calc(100% - 175px)',
    flexGrow: 1,
    padding: styleTheme.spacing(2),
    margin: '0 auto',
    paddingTop: '96px',
    [theme.breakpoints.up('md')]: {
      maxWidth: '960px',
    },
  },
}));

export default function MyApp(props: AppProps) {
  const classes = useStyles();

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <>
      <Head>
        <title>Stoklopen website</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
        <meta property="og:title" content="Stoklopen website" />
        <meta property="og:image" content="/bartimeus-og-image.png" />
        <meta property="og:url" content="https://hartonderderiem.bartimeus.nl" />
        <link rel="icon" type="image/png" href="/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="/favicon-160x160.png" sizes="160x160" />
        <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16" />
        <meta property="og:locale" content="nl_NL" />
      </Head>
      <ThemeProvider theme={theme}>
        <TableOfContentsProvider>
          <CssBaseline />
          <div className={classes.root}>
            <Drawer />
            <div className={classes.contentWrapper}>
              <main className={classes.content}>
                {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                <props.Component {...props.pageProps} />
                <BottomNavigation />
              </main>
              <TableOfContents />
            </div>
          </div>
        </TableOfContentsProvider>
      </ThemeProvider>
      <style jsx global>
        {`
          p,
          ul,
          ol {
            margin-top: 0;
            margin-bottom: 16px;
          }

          ol {
            font-size: 1rem;
            font-family: 'Roboto', 'Helvetica', 'Arial', sans-serif;
            font-weight: 400;
            line-height: 1.5;
            letter-spacing: 0.00938em;
            padding-bottom: 10px;
          }

          ul {
            padding-left: 30px;
          }

          a {
            color: ${theme.palette.secondary};
            text-decoration: none;
          }
        `}
      </style>
    </>
  );
}
