import React from 'react';
import { Button, makeStyles, Divider } from '@material-ui/core';
import { KeyboardArrowLeft, KeyboardArrowRight } from '@material-ui/icons';
import { useRouter } from 'next/dist/client/router';
import routes from '../Drawer/routes';

const useStyles = makeStyles(() => ({
  footer: {
    marginTop: '96px',
  },
  buttons: {
    margin: '24px 0px 32px',
    display: 'flex',
    justifyContent: 'space-between',
  },
  button: {
    fontWeight: 400,
    textTransform: 'none',
    padding: '8px 24px',
    fontSize: '0.9375rem',
  },
  icon: {
    margin: '0 8px',
  },
  noLeftButton: {
    margin: '24px 0px 32px',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  noRightButton: {
    margin: '24px 0px 32px',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
}));

export default function BottomNavigation() {
  const classes = useStyles();
  const router = useRouter();

  const routePath = router.pathname;
  let leftButton: string | undefined;
  let rightButton: string | undefined;

  routes.forEach((route, index) => {
    if (`/${route.name}` === routePath) {
      if (index !== 0) {
        leftButton = routes[index - 1].name;
      }

      if (index !== routes.length - 1) {
        rightButton = routes[index + 1].name;
      }
    }
  });

  let wrapperClasses = classes.buttons;
  if (leftButton && !rightButton) {
    wrapperClasses = classes.noRightButton;
  } else if (rightButton && !leftButton) {
    wrapperClasses = classes.noLeftButton;
  }

  function handleOnClick(e, path: string) {
    e.preventDefault();
    router.push(`/${path}#${path.toLowerCase()}`);
  }

  if (!leftButton && !rightButton) {
    return <></>;
  }

  return (
    <footer className={classes.footer}>
      <Divider aria-hidden="true" />
      <nav className={wrapperClasses}>
        {leftButton && (
          <Button
            className={classes.button}
            onClick={(e) => {
              handleOnClick(e, leftButton);
            }}
            aria-label={`Vorige hoofdstuk: ${leftButton}`}
          >
            <KeyboardArrowLeft className={classes.icon} />
            {leftButton}
          </Button>
        )}
        {rightButton && (
          <Button
            className={classes.button}
            onClick={(e) => {
              handleOnClick(e, rightButton);
            }}
            aria-label={`Volgende hoofdstuk: ${rightButton}`}
          >
            {rightButton}
            <KeyboardArrowRight className={classes.icon} />
          </Button>
        )}
      </nav>
    </footer>
  );
}
