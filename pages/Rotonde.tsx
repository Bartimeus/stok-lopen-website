import React, { useEffect } from 'react';
import { Typography } from '@material-ui/core';
import Description from '../src/components/typography/Description';
import Image from '../src/components/typography/image';
import H1 from '../src/components/typography/H1';
import { useTableOfContentsContext } from '../src/components/TableOfContents/context';

import Afbeeling1 from './afbeeldingen/rotonde/Afbeelding1.png?webp';
import H2 from '../src/components/typography/H2';

export default function Rotonde() {
  const tableOfContentsContext = useTableOfContentsContext();

  useEffect(() => {
    tableOfContentsContext.newContent([
      { name: 'Rotonde', link: '/Rotonde#rotonde' },
      { name: 'Verkennen rotonde', link: '/Rotonde#verkennen-rotonde' },
      { name: 'Aanleren rotonde', link: '/Rotonde#aanleren-rotonde' },
    ]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <H1>Rotonde</H1>
      <Description>Algemene verkenning van de rotonde (dit kan overigens ook gebruikt worden bij een kruispunt).</Description>
      <H2>Verkennen rotonde</H2>
      <Typography variant="body1">
        Als je een plattegrond hebt gemaakt van een rotonde met vier zijstraten kun je de zijstraten vergelijken met de uren van een klok
        (3, 6, 9 en 12 uur). Dit werkt voor veel mensen met een visuele beperking verduidelijkend. Elke rotonde is verschillend: vlucht
        heuvels ja of nee, aparte fietspaden ja of nee, een of twee richting verkeer.
      </Typography>
      <Typography variant="body1">We kijken naar deze plattegrond van een eenvoudige rotonde :</Typography>
      <Image src={Afbeeling1} alt="Tekening van een rotonde" />
      <Typography variant="body1">
        We gaan er nu voor de uitleg er vanuit dat de cliënt de rotonde altijd via straat 6 benaderd. De meeste cliënten lopen een vaste
        route en de straat die naar de rotonde leidt noemen we dus 6.
      </Typography>
      <Typography variant="body1">
        Als je de rotonde uitlegt let er dan op dat de cliënt steeds met zijn neus dezelfde richting blijft staan, hij kan dan gemakkelijker
        een total plaatje in zij hoofd maken Om een beeld van de rotonde te krijgen loopt je met de cliënt de rotonde rond.
      </Typography>
      <Typography variant="body1">
        Laat hem eventueel bij elke hoek weer in de oorspronkelijke richting staan. Wijs op elke hoek aan waar hij zich nu bevindt. De
        bedoeling van deze oefening/training is dat de cliënt zelf een beeld gaat opbouwen van hoe deze rotonde is opgebouwd of ingedeeld.
        Waar de cliënt zelf een beeld van opbouwt met jouw bevestiging is 10 x beter dan het beeld wat hij krijgt als jij het voor hem
        invult en beschrijft. Dat kun je ondersteunen met open vragen en hulpmiddelen als hand tekenen en de stoppen denken doen methode.
        Extra ondersteuning kan zijn : zelf een rondje lopen en op iedere bocht van de rotonde roepen waar ik sta (uiteraard kan dat alleen
        op een kleine rustige rotonde)
      </Typography>
      <H2>Aanleren rotonde</H2>
      <Typography variant="body1">
        Meestal steek je een rotonde het veiligst over met de klok mee . Kies altijd de veiligste weg omdat elke rotonde anders is ,kies
        voor de veiligste weg.
        <br /> Elke rotonde is verschillend; vlucht heuvels ja of nee, aparte fietspaden ja of nee. Eenrichtingsverkeer ja of nee
      </Typography>
      <Typography variant="body1">
        Vaak wordt gezegd dat onze cliënten kennis moeten hebben van ruimtelijke concepten. Wat is rechts en links, wat is voor of achter
        enz.? Voor mobiliteitsinstructie is het wel handig als we met deze concepten kunnen werken, maar dan is het vooral in het belang van
        de communicatie. Je kunt je afvragen of een cliënt zich beter kan oriënteren als hij recht en links kan benoemen en toepassen.
        Vanuit je eigen lichaam weten wat rechts en links is, is belangrijk. Onderzoek hoe een cliënt zijn richting bepaalt en benoemd .
        Links rechts. Deze andere kant. Hier kan je makkelijk achter komen door de cliënt een route te laten beschrijven. Je kunt dan de
        door hem gebruikte termen overnemen. Het is het meest handig als de cliënt kan klok kijken. We nemen nu voor de uitleg de meest
        eenvoudige rotonde en deze rotonde is goed voorzien van geleide lijnen om over te steken.
      </Typography>
      <Image src={Afbeeling1} alt="Tekening van een rotonde" />
      <Typography variant="body1">
        We benaderen de rotonde vanaf straat 6 en willen naar de supermarkt rechts bij straat 9. Zoals gezegd beschouwen we de rotonde als
        een klok met 4 straten op 3, 6, 9 en 12.
      </Typography>
      <ol>
        <li>
          bepaal aan welke kant van weg 6 je loopt. Als rechts dan moet je eerst straat 6 oversteken, dus links af slaan. De geleide lijn
          navigeert je naar het zebrapad. Bij de noppen tegels stel je je op om over te steken.
        </li>
        <li>
          na het oversteken sla je rechts af en loopt meteen links af straat 9 in. Het geluid van het verkeer en de echolocatie kan je hier
          bij helpen Als de cliënt na het oversteken niet meer weet of hij rechts of links maak hem dan attent op de verkeersgeluiden. Aan
          de hand daarvan kan hij bepalen welke kant hij op moet.
        </li>
        <li>We lopen straat 9 in, zoeken met de geleide lijn en noppen tegels en steken over.</li>
        <li>na het oversteken sla je links af en vervolgd je weg naar d e supermarkt.</li>
      </ol>
      <Typography variant="body1">
        Voor de terugweg vanaf de supermarkt start je met de klok in de handen bij straat 9. De cliënt loopt nu de tegenover gestelde
        richting, en dat is voor hem een nieuwe route Om een rotonde te kunnen overstekenmoet de cliënt uitstekende oriëntatie en
        navigatievaardigheden nodig hebben De instructeur biedt een continue begeleiding zonder daarbij al concrete invulling aan te geven.
      </Typography>
    </>
  );
}
