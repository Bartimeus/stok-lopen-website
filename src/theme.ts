import { createMuiTheme } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';

// Create a theme instance.
const theme = createMuiTheme({
  typography: {
    body1: {
      paddingBottom: '10px',
    },
  },
  palette: {
    primary: {
      main: '#008245',
    },
    secondary: {
      main: 'rgb(220, 0, 78)',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#fff',
    },
  },
});

export default theme;
