import React, { useEffect } from 'react';
import { Typography, List, ListItem, ListItemText } from '@material-ui/core';
import Description from '../src/components/typography/Description';
import Image from '../src/components/typography/image';
import H1 from '../src/components/typography/H1';
import H2 from '../src/components/typography/H2';
import { useTableOfContentsContext } from '../src/components/TableOfContents/context';

import Afbeelding2 from './afbeeldingen/gidslijn/Afbeelding2.png?webp';
import Afbeelding3 from './afbeeldingen/gidslijn/Afbeelding3.png?webp';
import Afbeelding4 from './afbeeldingen/gidslijn/Afbeelding4.png?webp';
import Afbeelding5 from './afbeeldingen/gidslijn/Afbeelding5.png?webp';
import Afbeelding6 from './afbeeldingen/gidslijn/Afbeelding6.png?webp';
import Afbeelding7 from './afbeeldingen/gidslijn/Afbeelding7.png?webp';

export default function Gidslijn() {
  const tableOfContentsContext = useTableOfContentsContext();

  useEffect(() => {
    tableOfContentsContext.newContent([
      { name: 'Gidslijn', link: '/Gidslijn#gidslijn' },
      { name: 'Gidslijn met diagonale techniek', link: '/Gidslijn#gidslijn-met-diagonale-techniek' },
      { name: 'Tweepuntstechniek', link: '/Gidslijn#tweepuntstechniek' },
    ]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <H1>Gidslijn</H1>
      <Description>
        Een gidslijn volgen is een manier om in een gewenste richting te lopen en zo dus de oriëntatie te behouden. Door systematisch
        contact te maken met de gidslijn zorgt u ervoor dat u altijd weet waar u zich bevindt.
      </Description>
      <Image src={Afbeelding2} alt="Man die over een gidslijn met een taststok loopt." />
      <Typography variant="body1">De hoofdlijn die wordt gevolgd kan verschillend zijn. Het kan zijn:</Typography>
      <List component="ol" type="1">
        <ListItem>
          <ListItemText>
            Tactiel (bijvoorbeeld een muur, een stoeprand of een aangelegde structuur)
            <Image src={Afbeelding3} alt="Man die met taststok langs een stoep loopt." />
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Auditief (bijv. geluid van het verkeer)
            <Image src={Afbeelding4} alt="Man luistert naar een vrachtwagen die langs rijdt." />
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Visueel (bijvoorbeeld een zebrapad of binnen lampen in het plafond,)
            <Image src={Afbeelding5} alt="Man met taststok die een zebrapad oversteekt." />
          </ListItemText>
        </ListItem>
      </List>
      <Typography variant="body1">
        Een gidslijn volgen kan worden uitgevoerd zonder hulpmiddelen, bijvoorbeeld met de hand langs een muur. Er kan ook gebruik gemaakt
        worden van elektronische hulpmiddelen of met behulp van een geleidehond.
      </Typography>
      <Typography variant="body1">
        Als u de taststok gebruikt kunt u de diagonale techniek of de pendeltechniek gebruiken, afhankelijk van de omgeving en het doel.
      </Typography>
      <H2>Gidslijn met diagonale techniek</H2>
      <Typography variant="body1">
        Hier laat De cliënt zien hoe je een muur als gidslijn kunt volgen, bijvoorbeeld om een deuropening te vinden.
      </Typography>
      <List>
        <ListItem>
          <ListItemText>De cliënt staat parallel aan de muur die ze wil volgen op ongeveer 20 cm afstand</ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Ze houdt de stok in haar hand aan andere kant dan waar de muur is en houdt de stok diagonaal voor het lichaam.
            <Image src={Afbeelding6} alt="Vrouw houdt de punt van de taststok tegen de muur." />
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>De punt van de stok wordt constant in contact gehouden met de muur.</ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>Het houden van de hand rond heuphoogte zorgt voor een goede reactietijd.</ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>Het kan helpen om in je hoofd een rechte lijn naar het doel voor te stellen als je loopt.</ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Bij een opening in de muur loop je rechtdoor en na enkele stappen wordt het contact met de muur hervat.
          </ListItemText>
        </ListItem>
      </List>
      <H2>Tweepuntstechniek</H2>
      <Typography variant="body1">
        Tweepuntstechniek wordt gebruikt bij het volgen van een gidslijn met de pendel techniek langs verticale oppervlakken (bijvoorbeeld
        een wand) of horizontale oppervlakken (bijvoorbeeld een grasrand of een aangelegde geleidelijn).
      </Typography>
      <List>
        <ListItem>
          <ListItemText>
            Als u op zoek bent naar ieen herkenningspunt langs een gidslijn aan de linkerkant, moet elke aazwaai van d estokpunt de
            linkerkant contact hebben met de gidslijn.
            <Image src={Afbeelding7} alt="Man houdt taststok voor zich en beweegt de taststok van links naar rechts." />
          </ListItemText>
        </ListItem>
        <ListItem>
          <ListItemText>
            Als het oppervlak horizontaal is, zoals deze aaqngelegde geleidelijn, kunt u tegelijkertijd de geleidelijn met de zolen van de
            voeten voelen.
          </ListItemText>
        </ListItem>
      </List>
    </>
  );
}
