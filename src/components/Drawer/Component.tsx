import React, { useState, useEffect } from 'react';
import { AppBar, Toolbar, IconButton, Drawer as MUIDrawer, makeStyles, List, ListItem, CssBaseline, Divider } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { useRouter } from 'next/dist/client/router';
import routes from './routes';

import bartimeusLogo from '../../../pages/afbeeldingen/bartimeus_logo.png?webp';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  drawer: {
    [theme.breakpoints.up('lg')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up('lg')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('lg')]: {
      display: 'none',
    },
  },
  drawerPaper: {
    width: drawerWidth,
    // [theme.breakpoints.down('md')]: {
    //   visibility: 'hidden',
    // },
  },
  headerTitleWrapper: {
    display: 'flex',
    minHeight: '56px',
    paddingLeft: '24px',
    paddingRight: '16px',
    [theme.breakpoints.up('md')]: {
      minHeight: '64px',
    },
    flexGrow: 1,
    alignItems: 'flexStart',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  headerTitle: {
    color: theme.palette.text.secondary,
    textDecoration: 'none',
    fontSize: '1rem',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    fontWeight: 500,
    lineHeight: 1.6,
    letterSpacing: '0.0075em',
  },
  listItem: {
    paddingLeft: 0,
  },
  link: {
    color: theme.palette.text.primary,
    padding: '6px 8px 6px 24px',
    fontSize: '0.875rem',
    fontWeight: 500,
    lineHeight: 1.5,
    letterSpacing: 0,
    width: '100%',
    height: '100%',
  },
  logo: {
    display: 'block',
    width: '139px',
  },
}));

export default function Drawer() {
  const classes = useStyles();
  const [mobileOpen, setMobileOpen] = useState(false);
  const [drawerVariant, setDrawerVariant] = useState<'temporary' | 'permanent' | 'persistent'>('temporary');
  const router = useRouter();

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const handleClick = (e, route: string) => {
    e.preventDefault();
    router.push(`/${route}`);
    setMobileOpen(false);
  };

  useEffect(() => {
    function updateVariant() {
      if (window.innerWidth >= 1280) {
        setDrawerVariant('permanent');
      } else {
        setDrawerVariant('temporary');
      }
    }
    window.addEventListener('resize', updateVariant);
    updateVariant();
    return () => window.removeEventListener('resize', updateVariant);
  }, []);

  const drawer = (
    <div>
      <div className={classes.headerTitleWrapper}>
        <a
          className={classes.headerTitle}
          href="/"
          onClick={(e) => {
            e.preventDefault();
            router.push('/');
            setMobileOpen(false);
          }}
        >
          <img className={classes.logo} src={bartimeusLogo} alt="Bartiméus logo" />
          Stoklopen
        </a>
      </div>
      <Divider />
      <List>
        {routes.map((route) => (
          <ListItem className={classes.listItem} button key={route.name}>
            <a
              className={classes.link}
              href={`/${route.name}`}
              onClick={(e) => {
                handleClick(e, route.name);
              }}
            >
              {route.name}
            </a>
          </ListItem>
        ))}
      </List>
    </div>
  );

  return (
    <>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="Open menu"
            aria-expanded={mobileOpen}
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="Hoofd">
        <MUIDrawer
          variant={drawerVariant}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          {drawer}
        </MUIDrawer>
      </nav>
    </>
  );
}
