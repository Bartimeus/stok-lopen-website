import React from 'react';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  imageWrapper: {
    // width: '100%',
    // padding: '24px',
    // [theme.breakpoints.up('sm')]: {
    //   borderRadius: '4px',
    //   backgroundColor: '#f5f5f5',
    // },
  },
  figcaption: {
    textAlign: 'center',
  },
  image: {
    width: '100%',
    maxWidth: '600px',
    display: 'block',
    position: 'relative',
    margin: '0 auto',
    paddingBottom: theme.spacing(1),
  },
}));

interface Props {
  src: string | undefined;
  alt: string;
}

export default function Image(props: Props) {
  const classes = useStyles();

  return (
    <figure className={classes.imageWrapper}>
      <img className={classes.image} alt={props.alt} src={props.src} />
      <figcaption aria-hidden="true" className={classes.figcaption}>
        {props.alt}
      </figcaption>
    </figure>
  );
}
