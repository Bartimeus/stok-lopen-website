import React from 'react';
import { makeStyles } from '@material-ui/core';
import theme from '../../theme';

const useStyles = makeStyles({
  body: {
    fontSize: '1rem',
    wordBreak: 'break-word',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    fontWeight: 400,
    lineHeight: 1.5,
    letterSpacing: '0.00938em',
    paddingBottom: theme.spacing(1),
  },
});

interface Props {
  children: string | undefined;
}

export default function Body(props: Props) {
  const classes = useStyles();

  return <p className={classes.body}>{props.children}</p>;
}
