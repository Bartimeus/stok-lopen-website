import React, { useEffect } from 'react';
import { Typography } from '@material-ui/core';
import Description from '../src/components/typography/Description';
import H1 from '../src/components/typography/H1';
import { useTableOfContentsContext } from '../src/components/TableOfContents/context';

import H2 from '../src/components/typography/H2';
import H3 from '../src/components/typography/H3';

export default function Rotonde() {
  const tableOfContentsContext = useTableOfContentsContext();

  useEffect(() => {
    tableOfContentsContext.newContent([
      { name: 'Begeleidingstechnieken', link: '/Begeleiden#begeleidingstechnieken' },
      { name: 'Algemene omgangsvormen', link: '/Begeleiden#algemene-omgangsvormen' },
      { name: 'Begeleidingsvormen', link: '/Begeleiden#begeleidingsvormen' },
    ]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <H1>Begeleidingstechnieken</H1>
      <Description>
        Als je slechtziend of blind bent, kan het prettig zijn om begeleiding te krijgen van iemand die goed ziet. Het is een veilige en
        effectieve manier van verplaatsing, vooral in een onbekende omgeving of bij tijdelijke veranderingen in een bekende omgeving.
        Wanneer een persoon met een visuele beperking door iemand die goed ziet wordt begeleid, is het belangrijk dat beiden besproken
        hebben hoe de begeleiding moet worden gedaan en hoe ze bijvoorbeeld moeten omgaan met deuren, trappen, enz.
        <br />
        In dit deel worden de verschillende begeleidings technieken beschreven.
        <br />
        Begeleidingstechnieken zijn bedoeld als een gelijkwaardige samenwerking tussen twee mensen. De technieken maken het mogelijk om in
        verschillende omgevingen samen te bewegen op een sociaal acceptabele. Veilige en effectieve manier.
        <br />
        De begeleider is verantwoordelijk voor beider veiligheid en zal dus altijd de veiligste positie kiezen voor beide.
        <br />
        Na enige tijd zult je waarschijnlijk enkele van de technieken aanpassen en daarbinnen uw eigen manier vinden. Veel mensen met een
        visuele beperking hebben een rest visus, die eventueel ngoed te gebruiken is bij het oriënteren. Sommige mensen hebben alleen
        &apos;s avonds, &apos;s nachts of bij slechte verlichting problemen met zien . Daarom hangt het af van de situatie of begeleiding
        wel of niet noodzakelijk is.
        <br />
        Het is een groot voordeel als de persoon met de visuele beperking de begeleidingstechnieken zo goed leert kennen dat hij of zij
        zelfs een onervaren begeleider kan instrueren hoe hij begeleid zou willen worden.
      </Description>
      <H2>Algemene omgangsvormen</H2>
      <Typography variant="body1">
        Ga er niet vanuit dat, de persoon die een visuele beperking en eventueel een gehoor beperking heeft (dit wordt verder in het
        document niet meer apart genoemd), je niet kan zien of horen! Alleen een klein percentage van de mensen is volledig blind of doof en
        ziet of hoort helemaal niets.
        <br />
        Realiseer je dat de cliënt door zijn beperking(en) het overzicht van zijn omgeving en situatie mist en al zijn mogelijkheden inzet
        om dit te compenseren.
        <br />
        Wanneer je een persoon met een visuele beperking groet, noem de naam van de persoon, noem je naam, introduceer eventuele andere
        mensen van je gezelschap en vertel waar deze staan. Bijvoorbeeld “Peter staat links van je.” Zorg voor een rustige omgeving, schakel
        achtergrondgeluid zoveel mogelijk uit Spreek duidelijk en rustig (overdrijf dit niet), controleer eventueel of de boodschap is
        overgekomen. Zorg dat je gezicht en lippen goed zichtbaar zijn. Spreek 1 op 1. Ga niet tussendoor met iemand anders praten.
        <br />
        Praat rechtstreeks tegen de persoon. Het heeft geen zin in de derde persoon te praten. Dit geldt ook voor mensen met een
        geleidehond: praat niet via de hond! Richt je tot de cliënt
        <br />
        Bespreek hoe je de cliënt eventueel waarschuwt bij onverwachte situatie of gevaar met bijvoorbeeld pas op plus toelichting of stop +
        toelichting.
        <br />
        Vertel dat je weg gaat of de ruimte verlaat, om te voorkomen dat de cliënt in een lege ruimte tegen niemand staat te praten.
        <br />
        Als je de persoon alleen gaat laten vraag dan wat de cliënt het meest prettig vindt.
        <br />
        Kortom: respecteer de zelfstandigheid van de persoon en vraag wat de ander nodig heeft aan begeleiding.
      </Typography>
      <H2>Begeleiden</H2>
      <Typography variant="body1">
        Het is belangrijk de cliënt op een zo goed mogelijke manier te begeleiden. Het doel van begeleiden is de cliënt veilig en effectief
        van A naar B te begeleiden. Het is belangrijk dat hij zich veilig voelt als hij met je meeloopt. Er is verschil tussen lopen en
        gezellig kletsen of lopen en de cliënt kennis laten maken met de omgeving zodat hij in de toekomst zelfstandig de route kan gaan
        lopen. Let op de fysieke belasting van jezelf en de cliënt, probeer zo ontspannen mogelijk te lopen. Door de cliënt iets achter je
        te laten lopen kan hij reageren op hoe jij je beweegt (stoep op/af-trap op/af)
      </Typography>
      <H3>De cliënt moet zelf een aantal dingen kunnen voor een veilige en goede begeleiding:</H3>
      <ol>
        <li>De cliënt zal actief moeten kunnen lopen in plaats van zich voort te laten trekken door, of te hangen aan de begeleider.</li>
        <li>stem het looptempo en houding op elkaar af. Let op de fysieke belasting van Zowel de cliënt als de begeleider.</li>
        <li>
          De cliënt moet mondelinge en/of niet-mondelinge aanwijzingen van de begeleider kunnen begrijpen. (Er moet communicatie mogelijk
          zijn)
        </li>
      </ol>
      <Typography variant="body1">Er zijn 3 regels bij het begeleiden:</Typography>
      <ol>
        <li>
          De cliënt houdt jou vast en jij niet de cliënt. Voordeel: Als hij jou vasthoudt moet hij zelf volgen en opletten. Houd jij de
          cliënt vast, dan kan hij volgen en hoeft hij niet zelf op te letten. Maak hieruit bewust een keuze.
          <br />
          <i>
            De cliënt kan (al naar gelang de doelgroep) bij de elleboog of schouder vasthouden. (Actief) of jou een hand of een arm
            geven(passief) of
          </i>
        </li>
        <li>
          Jij past je aan aan het tempo van de cliënt
          <br />
          <i>
            Dat betekent niet, dat je hem niet mag vragen door te lopen wanneer het tempo wel erg laag ligt. Als je maar niet gaat sleuren.
          </i>
        </li>
        <li>
          Wanneer de cliënt gewend is een stok te hanteren, neem deze altijd mee.
          <br />
          <i>
            Het is niet nodig dat de cliënt de stok ook daadwerkelijk gebruikt. Op beschermd terrein kan je daarvoor kiezen. Wanneer je in
            een winkelgebied of drukke straat loopt is het voldoende als de client de stok rechtop draagt als signaalfunctie. Het voordeel
            is dat je naast elkaar kan blijven lopen. De stok moet een vast attribuut worden van de cliënt net zoals zijn jas en tas.
          </i>
        </li>
      </ol>
      <H3>Er zijn verschillende manieren van begeleiden met daarbij een onderscheid:</H3>
      <ol type="A">
        <li>Begeleiden om een verplaatsing van A naar B te maken.</li>
        <li>Begeleiden om de route van A naar B te verkennen/aan te leren/ontdekken.</li>
      </ol>
      <Typography variant="body1">
        <strong>Begeleiding A</strong>
      </Typography>
      <Typography variant="body1">
        Hierbij ligt de aandacht op het bereiken van de bestemming en niet op het leren van een route. Wanneer het doel is dat de cliënt in
        de toekomst deze route zelfstandig gaat lopen heeft een actievere vorm van begeleiden de voorkeur.
      </Typography>
      <Typography variant="body1">
        <strong>Begeleiding B</strong>
      </Typography>
      <Typography variant="body1">
        Hierbij leert de cliënt zo zelfstandig als op dat moment mogelijk is naar het andere punt te lopen en leert informatie over de
        omgeving. Dit is goed voor de mobiliteitsvaardigheden van de cliënt en voor efficiënt (zoals geleerd bij de stok training)
        stokgebruik door de cliënt.(dit behoort tot het taken pakket begeleider B, instructeur C en D
      </Typography>
      <Typography variant="body1">Ook hier gaan we eerst uit van de 3 gouden regels:</Typography>
      <ol>
        <li>
          De cliënt houdt jou vast en jij niet de cliënt. Als hij jou vastpakt moet hij volgen en opletten, houd jij de hem vast dan kan hij
          volgen en hoeft hij niet op te letten.
        </li>
        <li>Jij past je aan aan het tempo van je cliënt.</li>
        <li>Wanneer de cliënt gewend is een stok te hanteren deze Altijd meenemen.</li>
      </ol>
      <Typography variant="body1">
        <strong>en verder</strong>
      </Typography>
      <Typography variant="body1">
        Vraag aan de cliënt hoe hij begeleidt wil worden
        <br />
        loop op onbekend terrein in een rustig tempo
        <br />
        Start als het kan stilstaand vanaf (voor de cliënt) een bekend punt.
        <br />
        Laat de cliënt over de ribbellijnen lopen, indien aanwezig, en laat hem (eventueel) benoemen wanneer hij de
        rubbertegels/noppentegels voelt. Maak hem attent op richtingsveranderingen en opvallende punten zoals huizen, muren etc. waar hij
        langs loopt. Snij geen stukjes af als je te laat bent, maar loop de route zoals de cliënt die later ook (eventueel zelfstandig) zal
        lopen.
        <br />
        Dreigt er gevaar, waarschuw dan niet alleen met het afgesproken woordje &ldquo;stop&ldquo; of &ldquo;pas op&ldquo; maar zeg ook wat
        de cliënt moet doen en waarom. Dit voorkomt panieksituaties. Dus bijvoorbeeld: stop: sta stil of pas op; buk De stok moet op de
        breedte van het lichaam rustig van links naar rechts heen en weer gezwaaid worden. Rolpunt moet altijd op de grond blijven.
      </Typography>
      <Typography variant="body1">
        Overigens geldt het bovenstaande niet alleen voor cliënten die met een stok lopen. Ook cliënten die zonder stok lopen hebben baat
        bij vroegtijdige, eenduidige aanwijzingen en begeleiding. Het is belangrijk dat de routes altijd op dezelfde manier gelopen wordt
        door alle begeleiders{' '}
      </Typography>
      <H3>Lengteverschil begeleider en cliënt</H3>
      <Typography variant="body1">
        Er kunnen situaties zijn waarin normale begeleidende technieken niet kunnen worden gebruikt, maar in plaats daarvan de techniek moet
        worden aangepast aan de omstandigheden.
      </Typography>
      <Typography variant="body1">
        Als de persoon met de visuele beperking aanzienlijk langer is dan de begeleider kan hij zijn hand op de schouder van de begeleider
        leggen in plaats van dat hij de arm vasthoudt.
      </Typography>
      <Typography variant="body1">
        De begeleider moet speciale aandacht besteden aan obstakels op hoofdhoogte om ervoor te zorgen dat de persoon met de visuele
        beperking niet dingen zoals luifels, takken of zijspiegels van vrachtwagens raakt.
      </Typography>
      <Typography variant="body1">
        Als de persoon met de visuele beperking aanzienlijk kleiner is dan de begeleider, kan de begeleider zijn arm strekken en zijn pols
        of een paar vingers laten vasthouden (als het een kind is).
      </Typography>
      <Typography variant="body1">
        Als de begeleider relatief grote bovenarmen heeft, kan de persoon met de visuele beperking de mouw of de jas op een geschikte plaats
        vasthouden.
      </Typography>
      <Typography variant="body1">
        In geval van extra ondersteuning, bijvoorbeeld slecht balans gebruik dan de eerder in deze app genoemde begeleidings technieken,(
        bijvoorbeeld bij traplopen)
      </Typography>
      <H2>Begeleidingsvormen, Foto&apos;s van verschillende vormen begeleiding</H2>
      <H3>Contact maken</H3>
      <Typography variant="body1">
        Als er contact is tussen een begeleider en een persoon met een visuele beperking, kan dat op initiatief van beide partijen. Als de
        begeleider contact maakt: De onderstaande fotoserie laat zien hoe de begeleider, na zichzelf mondeling te hebben aangekondigd, naast
        Lykke staat en met de rug van haar eigen hand de rug van de hand van de cliënt aanraakt De cliënt laat haar hand over de arm van de
        begeleider glijden tot over de elleboog en houdt de elleboog vast ( elleboog grip)
      </Typography>
    </>
  );
}
