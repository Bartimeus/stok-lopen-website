import React, { useContext, useState } from 'react';

interface Content {
  name: string;
  link: string;
}

function useTableOfContentsContextProviderValue() {
  const [content, setContent] = useState<Content[]>([]);

  return {
    content,
    newContent: (newContent: Content[]) => setContent(newContent),
  };
}

const TableOfContentsContext = React.createContext<null | ReturnType<typeof useTableOfContentsContextProviderValue>>(null);

interface Props {
  children: React.ReactNode;
}
export function TableOfContentsProvider(props: Props) {
  const tableOfContentsContextProviderValue = useTableOfContentsContextProviderValue();

  return <TableOfContentsContext.Provider value={tableOfContentsContextProviderValue}>{props.children}</TableOfContentsContext.Provider>;
}

export function useTableOfContentsContext() {
  const c = useContext(TableOfContentsContext);

  if (!c) {
    throw Error('Please wrap your component inside a <RecordingsProvider /> before using its context.');
  }

  return c;
}

export const { Consumer } = TableOfContentsContext;
