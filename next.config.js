// next.config.js
const withPlugins = require('next-compose-plugins');
const optimizedImages = require('next-optimized-images');

module.exports = withPlugins([
  [
    optimizedImages,
    {
      imagesFolder: 'afbeeldingen',
      imagesName: '[name]-[hash].[ext]',
      optimizeImages: true,
      /* config for next-optimized-images */
    },
  ],

  // your other plugins here
]);
