import React from 'react';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  H1: {
    margin: '16px 0',
    fontSize: '40px',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    fontWeight: 400,
    lineHeight: 1.167,
    letterSpacing: '0em',
    wordBreak: 'break-word',
  },
  anchorLink: {
    marginTop: '-96px',
    position: 'absolute',
  },
});

interface Props {
  children: string | undefined;
}

export default function H1(props: Props) {
  const classes = useStyles();
  const anchorLinkText = props.children.toLowerCase().replace(/ /g, '-');

  return (
    <h1 className={classes.H1}>
      {/* eslint-disable-next-line jsx-a11y/anchor-is-valid, jsx-a11y/anchor-has-content */}
      <a className={classes.anchorLink} id={anchorLinkText} aria-hidden="true" />
      {props.children}
    </h1>
  );
}
